# hello_flutter

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference

## Preparation
> This section could be shifted to a cookbook
### Learning dart
#### Resources/doc
- [Dart reference sheet](https://dart.dev/guides/language/specifications/DartLangSpec-v2.2.pdf)
- [Dart 1.x reference sheet](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-408.pdf)
- [Dart: learnXinYminutes](https://learnxinyminutes.com/docs/dart/)